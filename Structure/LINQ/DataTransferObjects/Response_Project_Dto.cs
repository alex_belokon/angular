﻿using Structure.Core.Models;

namespace LINQ.DataTransferObjects
{
    public class Response_Project_Dto
    {
        public Project Project { get; set; }
        public Task TaskByDescription { get; set; }
        public Task TaskByName { get; set; }
        public int UsersCount { get; set; }
    }
}

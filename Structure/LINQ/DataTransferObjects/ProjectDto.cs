﻿using System;
using System.Collections.Generic;

namespace LINQ.DataTransferObjects
{
    public class ProjectDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public string Image { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public UserDto User { get; set; }
        public TeamDto Team { get; set; }
        //public List<Task> ProjectTasks { get; set; }
        public ICollection<TaskDto> Tasks { get; set; }
    }
}

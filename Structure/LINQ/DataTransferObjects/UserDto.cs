﻿using System;
using System.Collections.Generic;

namespace LINQ.DataTransferObjects
{
    public class UserDto
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? Position { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public TeamDto Team { get; set; }
        public ICollection<TaskDto> Tasks { get; set; }
        public ICollection<ProjectDto> Projects { get; set; }
    }
}

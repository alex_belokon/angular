﻿
using System;
using Structure.Core.Models;


namespace LINQ.DataTransferObjects
{
    public class TaskDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public UserDto User { get; set; }
        public ProjectDto Project { get; set; }
    }
}

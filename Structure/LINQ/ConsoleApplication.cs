﻿using EasyConsole;
using LINQ.Pages;
using System.Net.Http;

namespace LINQ
{
    class ConsoleApplication : Program
    {
        private readonly HttpClient _client;
        public ConsoleApplication(HttpClient httpClient) : base("LINQ", breadcrumbHeader: true)
        {
            _client = httpClient;

            AddPage(new MainPage(this));
            AddPage(new StartPage(this, httpClient));
            AddPage(new Exit(this));

            SetPage<MainPage>();
        }
    }
}

﻿using EasyConsole;
using LINQ.Services;
using Newtonsoft.Json;
using Structure.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LINQ.DataTransferObjects;
using MTask = Structure.Core.Models.Task;
using Task = System.Threading.Tasks.Task;

namespace LINQ.Pages
{
    class StartPage : Page
    {
        private readonly HttpClient _client;
        public QueryService QueryService = new QueryService();
        public StartPage(Program program, HttpClient httpClient)
            : base("Start Page", program)
        {
            _client = httpClient;
        }

        public override async void Display()
        {
            base.Display();

            //var markedTaskId = await QueryService.MarkRandomTaskWithDelay(1000); Рабочий метод
            //Я понимаю что нужно было сделать без Result, но у меня валится тогда консольное приложение,
                //                      по срокам  я не успевал переделать все приложение или пофиксить это не удалось к сожалению(
            var markedTaskId = QueryService.MarkRandomTaskWithDelay(1000).Result;

                Console.WriteLine(markedTaskId);



                var responseProjects = string.Empty;
            var responseTeams = string.Empty;
            var responseUsers = string.Empty;
            var responseTasks = string.Empty;

            Task allTasks = null;

            try
            {
                var responseProjectsTask = _client.GetStringAsync(Settings.APP_PATH + "/api/Projects");
                var responseTeamsTask = _client.GetStringAsync(Settings.APP_PATH + "/api/Teams");
                var responseUsersTask = _client.GetStringAsync(Settings.APP_PATH + "/api/Users");
                var responseTasksTask = _client.GetStringAsync(Settings.APP_PATH + "/api/Tasks");

                allTasks = Task.WhenAll(responseProjectsTask, responseTeamsTask, responseUsersTask, responseTasksTask);

                //await allTasks; Рабочий метод.   Я понимаю что нужно было сделать без Wait, но у меня валится тогда консольное приложение,
                //                      по срокам  я не успевал переделать все приложение или пофиксить это не удалось к сожалению( 
                allTasks.Wait();

                responseProjects = await responseProjectsTask;
                responseTeams = await responseTeamsTask;
                responseUsers = await responseUsersTask;
                responseTasks = await responseTasksTask;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                Console.WriteLine("IsFaulted: " + allTasks.IsFaulted);
                foreach (var inx in allTasks.Exception.InnerExceptions)
                {
                    Console.WriteLine("InnerException: " + inx.Message);
                }
            }


            var listProjects = JsonConvert.DeserializeObject<List<Project>> (responseProjects);
            var listTeams = JsonConvert.DeserializeObject<List<Team>>(responseTeams);
            var listUsers = JsonConvert.DeserializeObject<List<User>>(responseUsers);
            var listTasks = JsonConvert.DeserializeObject<List<MTask>>(responseTasks);



            if (listProjects != null && responseTeams != null && responseUsers !=null && responseTasks != null)
            {
                var innerUsers = from user in listUsers
                                 join team in listTeams on user.TeamId equals team.Id into ut
                    from subteam in ut.DefaultIfEmpty()
                    select new User {
                        Id = user.Id,
                        Birthday = user.Birthday,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        RegisteredAt = user.RegisteredAt,
                        TeamId = user.TeamId,
                        Team = subteam ?? null
                    };

                Entities.Users = innerUsers.ToList();

                var innerTasks = listTasks.Join(innerUsers,
                    p => p.PerformerId,
                    u => u.Id,
                    (p, u) => new MTask
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        CreatedAt = p.CreatedAt,
                        FinishedAt = p.FinishedAt,
                        ProjectId = p.ProjectId,
                        State = p.State,
                        PerformerId = p.PerformerId,
                        User = u
                    }).ToList();

                Entities.Projects  = listProjects.Join(listTeams,
                    p => p.TeamId,
                    t => t.Id,
                    (p, t) => new Project
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        CreatedAt = p.CreatedAt,
                        Deadline = p.Deadline,
                        TeamId = p.TeamId,
                        Team = t,
                        AuthorId = p.AuthorId
                    }).Join(innerUsers,
                    p => p.AuthorId,
                    u => u.Id,
                    (p, u) => new Project
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        CreatedAt = p.CreatedAt,
                        Deadline = p.Deadline,
                        TeamId = p.TeamId,
                        Team = p.Team,
                        AuthorId = p.AuthorId,
                        User = u
                    }).GroupJoin(innerTasks,
                    p => p.Id,
                    t => t.ProjectId,
                    (p, t) => new Project
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        CreatedAt = p.CreatedAt,
                        Deadline = p.Deadline,
                        TeamId = p.TeamId,
                        Team = p.Team,
                        AuthorId = p.AuthorId,
                        User = p.User,
                        Tasks = t.ToList()
                    }).ToList();
            }

            if (Entities.Projects != null && Entities.Users != null)
            {
                var allUsers = Entities.Users.Select(x => x.FirstName + " " + x.LastName).ToList();
                var userName = Input.ReadList("Select a user", allUsers);
                Output.WriteLine(ConsoleColor.Green, "You selected {0}", userName);

                var userId = Entities.Users.Where(x => x.FirstName == userName.Split(' ')[0].ToString() && x.LastName == userName.Split(' ')[1].ToString()).FirstOrDefault().Id;

                Task TasksFromMethod = null;

                var queryCountTasksOfProject = new Dictionary<Project,int>();
                var queryListTasksByUserId = new List<MTask>();
                var queryFinishedTasksByUserId = new List<KeyValuePair<int, string>>();
                var queryListTeamByUserOver10Old = new List<Team_Dto>();
                var queryListUsersWithTasks = new Dictionary<User, List<MTask>>();
                var queryStruct = new Response_Dto();
                var queryStructByProject = new List<Response_Project_Dto>();

                try
                {
                    var queryCountTasksOfProjectTask = QueryService.GetCountTasksOfProjectAsync(userId);

                    var queryListTasksByUserIdTask = QueryService.GetListTasksByUserIdAsync(userId);

                    var queryFinishedTasksByUserIdTask = QueryService.GetIdNameFinishedTasksByUserIdAsync(userId);

                    var queryListTeamByUserOver10OldTask = QueryService.GetListTeamByUserOver10OldAsync();

                    var queryListUsersWithTasksTask = QueryService.GetListUsersWithTasksAsync();

                    var queryStructTask = QueryService.GetStructAsync(userId);

                    var queryStructByProjectTask = QueryService.GetStructByProjectAsync();

                    TasksFromMethod = Task.WhenAll(queryCountTasksOfProjectTask, queryListTasksByUserIdTask, queryFinishedTasksByUserIdTask,
                        queryListTeamByUserOver10OldTask, queryListUsersWithTasksTask, queryStructTask, queryStructByProjectTask);

                    //await TasksFromMethod; Рабочий метод.  Я понимаю что нужно было сделать без Wait, но у меня валится тогда консольное приложение,
                    //                      по срокам  я не успевал переделать все приложение или пофиксить это не удалось к сожалению( 
                    TasksFromMethod.Wait();

                    queryCountTasksOfProject = await queryCountTasksOfProjectTask;
                    queryListTasksByUserId = await queryListTasksByUserIdTask;
                    queryFinishedTasksByUserId = await queryFinishedTasksByUserIdTask;
                    queryListTeamByUserOver10Old = await queryListTeamByUserOver10OldTask;
                    queryListUsersWithTasks = await queryListUsersWithTasksTask;
                    queryStruct = await queryStructTask;
                    queryStructByProject = await queryStructByProjectTask;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: " + ex.Message);
                    Console.WriteLine("IsFaulted: " + TasksFromMethod.IsFaulted);
                    foreach (var inx in TasksFromMethod.Exception.InnerExceptions)
                    {
                        Console.WriteLine("InnerException: " + inx.Message);
                    }
                }




                Output.WriteLine(ConsoleColor.Green, "List Tasks By UserId:");
                foreach (var task in queryListTasksByUserId)
                {
                    Output.WriteLine(ConsoleColor.Green, "TaskId: {0}, Name: {1}, Description: {2}, CreatedAt: {3},  FinishedAt: {4}", task.Id, task.Name, task.Description, task.CreatedAt, task.FinishedAt);
                }
                Output.WriteLine(ConsoleColor.Red, "---------------------------------------------");

                Output.WriteLine(ConsoleColor.Green, "Count Tasks Of Project:");
                foreach (var pair in queryCountTasksOfProject)
                {
                    Output.WriteLine(ConsoleColor.Green, "ProjectId: {0}, Project name: {1} - Tasks {2}", pair.Key.Id, pair.Key.Name, pair.Value);
                }
                Output.WriteLine(ConsoleColor.Red, "---------------------------------------------");

                Output.WriteLine(ConsoleColor.Green, "Finished Tasks By UserId:");
                foreach (var pair in queryFinishedTasksByUserId)
                {
                    Output.WriteLine(ConsoleColor.Green, "TaskId: {0} - TaskName: {1}", pair.Key, pair.Value);
                }
                Output.WriteLine(ConsoleColor.Red, "---------------------------------------------");

                Output.WriteLine(ConsoleColor.Green, "List Team ByUser Over 10 Old:");
                foreach (var team in queryListTeamByUserOver10Old)
                {
                    Output.WriteLine(ConsoleColor.Green, "TeamId: {0}, TeamName: {1}, UserId: {2}", team.Id, team.Name, team.User.Id);
                }
                Output.WriteLine(ConsoleColor.Red, "---------------------------------------------");

                Output.WriteLine(ConsoleColor.Green, "List Users With Tasks:");
                foreach (var pair in queryListUsersWithTasks)
                {
                    Output.WriteLine(ConsoleColor.Green, "UserId: {0}, UserName: {1}, CountTasks: {2} ", pair.Key.Id, pair.Key.FirstName, pair.Value.Count);
                }
                Output.WriteLine(ConsoleColor.Red, "---------------------------------------------");

                Output.WriteLine(ConsoleColor.Green, "Struct:");
                Output.WriteLine(ConsoleColor.Green, "UserId: {0}, CountOfCanceledTasks: {1}, CountOfTasks: {2}, LongTaskId: {3}, ProjectId: {4}", queryStruct.User.Id, queryStruct.CountOfCanceledTasks, queryStruct.CountOfTasks,
                    queryStruct.LongTask.Id, queryStruct.Project.Id);
                Output.WriteLine(ConsoleColor.Red, "---------------------------------------------");

                Output.WriteLine(ConsoleColor.Green, "Struct By Project:");
                foreach (var item in queryStructByProject)
                {
                    Output.WriteLine(ConsoleColor.Green, "TaskByName: {0}, TaskByDescription: {1}, UsersCount: {2}, ProjectId: {3}",
                        item.TaskByName == null ? 0 : item.TaskByName.Id, item.TaskByDescription == null ? 0 : item.TaskByDescription.Id, item.UsersCount, item.Project.Id);
                }
                Output.WriteLine(ConsoleColor.Red, "---------------------------------------------");
            }

            Input.ReadString("Press [Enter] to navigate home");
            Program.NavigateHome();
        }
    }
}

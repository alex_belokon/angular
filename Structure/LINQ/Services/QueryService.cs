﻿using LINQ.DataTransferObjects;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Newtonsoft.Json;
using Structure.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;
using MTask = Structure.Core.Models.Task;
using Task = System.Threading.Tasks.Task;

namespace LINQ.Services
{
    public class QueryService
    {
        private Timer _timer;
        private readonly HttpClient _client;
        public QueryService()
        {
            _client = new HttpClient();
        }

        public async Task<Dictionary<Project, int>> GetCountTasksOfProjectAsync(int userId)
        {
            return await Task.Run(() =>
                Entities.Projects.Where(x => x.AuthorId == userId).ToDictionary(k => k, k => k.Tasks.Count()));
        }

        public async Task<List<MTask>> GetListTasksByUserIdAsync(int userId)
        {
            return await Task.Run(() =>
                Entities.Projects.SelectMany(p => p.Tasks, (p, t) => t)
                    .Where(x => x.PerformerId == userId && x.Name.Length < 45).ToList());
        }

        public async Task<List<KeyValuePair<int, string>>> GetIdNameFinishedTasksByUserIdAsync(int userId)
        {
            return await Task.Run(() => Entities.Projects.SelectMany(p => p.Tasks, (p, t) => t)
                .Where(x => x.PerformerId == userId && x.State == TaskState.Finished &&
                            x.FinishedAt.Year == DateTime.Now.Year)
                .Select(t => new KeyValuePair<int, string>(t.Id, t.Name)).ToList());
        }

        public async Task<List<Team_Dto>> GetListTeamByUserOver10OldAsync()
        {

            return await Task.Run(() => Entities.Users
                .Where(x => x.Birthday.Year < DateTime.Now.Year - 10 && x.Team != null)
                .OrderByDescending(o => o.RegisteredAt)
                .Select(u => new {Id = u.Team.Id, Name = u.Team.Name, User = u})
                .GroupBy(g => g.Name)
                .SelectMany(v => v).Select(k => new Team_Dto {Id = k.Id, Name = k.Name, User = k.User}).ToList());

        }

        public async Task<Dictionary<User, List<MTask>>> GetListUsersWithTasksAsync()
        {
            return await Task.Run(() => Entities.Projects.SelectMany(p => p.Tasks, (p, t) => t)
                .OrderByDescending(o => o.Name.Length)
                .GroupBy(u => u.User).OrderBy(o => o.Key.FirstName)
                .ToDictionary(g => g.Key, tasks => tasks.Select(h => h).ToList()));
        }

        public async Task<Response_Dto> GetStructAsync(int userId)
        {
            Response_Dto response = new Response_Dto();
            Task tasks = null;

            try
            {
                Task t1 = Task.Run(() =>
                {
                    var project = Entities.Projects.Where(x => x.User.Id == userId).OrderBy(o => o.CreatedAt)
                        .LastOrDefault();
                    if (project != null)
                    {
                        response.Project = project;
                        response.User = project.User;
                        response.CountOfTasks = project.Tasks.Count();
                    }
                });

                Task t2 = Task.Run(() => response.CountOfCanceledTasks = Entities.Projects
                    .SelectMany(p => p.Tasks, (p, t) => t)
                    .Where(x => x.PerformerId == userId && x.State == TaskState.Canceled).Count());

                Task t3 = Task.Run(() => response.LongTask = Entities.Projects.SelectMany(p => p.Tasks, (p, t) => t)
                    .Where(x => x.PerformerId == userId)
                    .Select(x => new {Task = x, Duration = x.FinishedAt - x.CreatedAt})
                    .OrderByDescending(d => d.Duration)
                    .FirstOrDefault()?.Task);

                tasks = Task.WhenAll(t1, t2, t3);
                await tasks;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                Console.WriteLine("IsFaulted: " + tasks.IsFaulted);
                foreach (var inx in tasks.Exception.InnerExceptions)
                {
                    Console.WriteLine("Inner Exception: " + inx.Message);
                }


            }

            return response;

        }

        public async Task<List<Response_Project_Dto>> GetStructByProjectAsync()
        {
            return await Task.Run(() => Entities.Projects.Select(x => new Response_Project_Dto
            {
                Project = x,
                TaskByDescription = x.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                TaskByName = x.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                UsersCount = Entities.Users
                    .Where(u => u.TeamId == x.TeamId && (x.Description.Length > 20 || x.Tasks.Count() < 3)).Count()
            }).ToList());
        }

        public Task<int> MarkRandomTaskWithDelay(int delay)
        {

            var tcs = new TaskCompletionSource<int>();
            Random rnd = new Random();
            Task.Run(() => {
                try
                {
                        _timer = new Timer(delay);
                        _timer.Elapsed += async (sender, e) => await HandleTimer();
                        _timer.AutoReset = false;
                        _timer.Enabled = true;

                        async Task HandleTimer()
                        {
                            var responseTasks = await _client.GetStringAsync(Settings.APP_PATH + "/api/Tasks");

                            var listTasks = JsonConvert.DeserializeObject<List<MTask>>(responseTasks);

                            if (listTasks != null)
                            {
                                var task = listTasks.OrderBy(x => rnd.Next(listTasks.Count)).FirstOrDefault();
                                task.State = TaskState.Finished;
                                var responseMessage = await _client.PutAsJsonAsync(Settings.APP_PATH + "/api/Tasks", task);
                                if (responseMessage.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    tcs.SetResult(task.Id);
                                }
                                else
                                {
                                    throw new Exception("Bad HttpRequest to update the task");
                                }
                            }
                        }
                    
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }

            });
            return tcs.Task;

        }
    }
}

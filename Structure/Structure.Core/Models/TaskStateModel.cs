﻿namespace Structure.Core.Models
{
    public class TaskStateModel
    {
        public int Id { get; set; }
        public string? Value { get; set; }
    }
}

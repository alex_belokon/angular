﻿using System.Collections.Generic;

namespace Structure.Core.Models
{
    public static class Entities
    {
        public static List<Project> Projects { get; set; }
        public static List<User> Users { get; set; }
    }
}

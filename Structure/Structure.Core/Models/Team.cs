﻿using System;
using System.Collections.Generic;
using Structure.Core.Interfaces.Repository;

namespace Structure.Core.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Color { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<Project> Projects { get; set; }
        public ICollection<User> Users { get; set; }
    }
}

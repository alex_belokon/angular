﻿using Structure.Core.Models;

namespace Structure.Core.Interfaces.Repository
{
    public interface ITeamRepository : IRepositoryBase<Team>
    {
    }
}

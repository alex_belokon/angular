﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Structure.Core.Models;
using Task = System.Threading.Tasks.Task;

namespace Structure.Core.Interfaces.Business
{
    public interface IUserManager
    {
        Task<IEnumerable<User>> GetUsersAsync();
        Task<User> GetUserByIdAsync(int userId);
        Task CreateAsync(User user);
        Task UpdateAsync(User user);
        Task DeleteAsync(User user);
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MTask = Structure.Core.Models.Task;

namespace Structure.Core.Interfaces.Business
{
    public interface ITaskManager
    {
        Task<IEnumerable<MTask>> GetTasksAsync();
        Task<MTask> GetTaskByIdAsync(int taskId);
        Task CreateAsync(MTask task);
        Task UpdateAsync(MTask task);
        Task DeleteAsync(MTask task);
    }
}

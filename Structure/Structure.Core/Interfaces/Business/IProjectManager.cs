﻿using System.Collections.Generic;
using Structure.Core.Models;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Structure.Core.Interfaces.Business
{
    public interface IProjectManager
    {
        Task<IEnumerable<Project>> GetProjectsAsync();
        Task<Project> GetProjectByIdAsync(int projectId);
        Task CreateAsync(Project project);
        Task UpdateAsync(Project project);
        Task DeleteAsync(Project project);
    }
}

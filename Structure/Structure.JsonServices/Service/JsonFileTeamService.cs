﻿using Microsoft.AspNetCore.Hosting;
using Structure.Core.Models;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace Structure.JsonServices.Service
{
    public class JsonFileTeamService
    {
        private string JsonFileName
        {
            get { return ("..\\Structure.JsonServices\\data\\teams.json"); }
        }

        
        public IEnumerable<Team> GetTeams()
        {
            using (var jsonFileReader = File.OpenText(JsonFileName))
            {
                return JsonSerializer.Deserialize<Team[]>(jsonFileReader.ReadToEnd(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });
            }
        }
    }
}

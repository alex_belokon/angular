﻿using Microsoft.AspNetCore.Hosting;
using Structure.Core.Models;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace Structure.JsonServices.Service
{
    public class JsonFileTaskService
    {
        private string JsonFileName
        {
            get { return ("..\\Structure.JsonServices\\data\\tasks.json"); }
        }

        public IEnumerable<Task> GetTasks()
        {
            using (var jsonFileReader = File.OpenText(JsonFileName))
            {
                return JsonSerializer.Deserialize<Task[]>(jsonFileReader.ReadToEnd(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });
            }
        }
    }
}

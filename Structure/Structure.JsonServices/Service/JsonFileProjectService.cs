﻿using Microsoft.AspNetCore.Hosting;
using Structure.Core.Models;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace Structure.JsonServices.Service
{
    public class JsonFileProjectService
    {
        private string JsonFileName
        {
            get { return ( "..\\Structure.JsonServices\\data\\projects.json"); }
        }

        public IEnumerable<Project> GetProjects()
        {
            using (var jsonFileReader = File.OpenText(JsonFileName))
            {
                return JsonSerializer.Deserialize<Project[]>(jsonFileReader.ReadToEnd(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });
            }
        }
    }
}

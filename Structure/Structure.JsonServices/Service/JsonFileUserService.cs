﻿using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using Structure.Core.Models;

namespace Structure.JsonServices.Service
{
    public class JsonFileUserService
    {
        private string JsonFileName
        {
            get { return ("..\\Structure.JsonServices\\data\\users.json"); }
        }


        public IEnumerable<User> GetUsers()
        {
            using (var jsonFileReader = File.OpenText(JsonFileName))
            {
                return JsonSerializer.Deserialize<User[]>(jsonFileReader.ReadToEnd(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });
            }
        }
    }
}

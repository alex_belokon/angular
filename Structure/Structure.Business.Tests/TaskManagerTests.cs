using Structure.Business.Managers;
using System;
using Microsoft.EntityFrameworkCore;
using Structure.Data.Repository;
using Xunit;
using Structure.Data.Context;

namespace Structure.Business.Tests
{
    public class TaskManagerTests
    {
        private readonly TaskManager _taskManager;
        private readonly TaskRepository _taskRepository;
        private readonly StructureDbContext _dataContext;
        public TaskManagerTests()
        {
            var dbContextOptions = new DbContextOptionsBuilder<StructureDbContext>()
                .UseSqlServer("server=(localdb)\\MSSQLLocalDB; database=Structure; Integrated Security=true");
            _dataContext = new StructureDbContext(dbContextOptions.Options);
            _taskRepository = new TaskRepository(_dataContext);
            _taskManager = new TaskManager(_taskRepository);
        }

        [Fact]
        public void GetTasks_WhenStart_Then200()
        {
            Assert.Equal(200, _taskManager.GetTasks().Length);

        }
    }
}

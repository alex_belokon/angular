using Structure.Business.Managers;
using System;
using Microsoft.EntityFrameworkCore;
using Structure.Data.Repository;
using Xunit;
using Structure.Data.Context;

namespace Structure.Business.Tests
{
    public class UserManagerTests
    {
        private readonly UserManager _userManager;
        private readonly UserRepository _userRepository;
        private readonly StructureDbContext _dataContext;
        public UserManagerTests()
        {
            var dbContextOptions = new DbContextOptionsBuilder<StructureDbContext>()
                .UseSqlServer("server=(localdb)\\MSSQLLocalDB; database=Structure; Integrated Security=true");
            _dataContext = new StructureDbContext(dbContextOptions.Options);
            _userRepository = new UserRepository(_dataContext);
            _userManager = new UserManager(_userRepository);
        }

        [Fact]
        public void GetUsers_WhenStart_Then50()
        {
            Assert.Equal(50, _userManager.GetUsers().Length);

        }
    }
}

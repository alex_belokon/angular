using Structure.Business.Managers;
using System;
using Microsoft.EntityFrameworkCore;
using Structure.Data.Repository;
using Xunit;
using Structure.Data.Context;
using Structure.Core.Models;

namespace Structure.Business.Tests
{
    public class ProjectManagerTests
    {
        private readonly ProjectManager _projectManager;
        private readonly ProjectRepository _projectRepository;
        private readonly StructureDbContext _dataContext;
        public ProjectManagerTests()
        {
            var dbContextOptions = new DbContextOptionsBuilder<StructureDbContext>()
                .UseSqlServer("server=(localdb)\\MSSQLLocalDB; database=Structure; Integrated Security=true");
            _dataContext = new StructureDbContext(dbContextOptions.Options);
            _projectRepository = new ProjectRepository(_dataContext);
            _projectManager = new ProjectManager(_projectRepository);
        }

        [Fact]
        public void GetProjects_WhenStart_Then100()
        {
            Assert.Equal(100, _projectManager.GetProjects().Length);

        }

        [Fact]
        public void Create_WhenNewProject_ThenProjectAddToDB()
        {
            var project = new Project() { AuthorId = 25, CreatedAt = DateTime.Now, Deadline = DateTime.Now.AddDays(2), Description = "Test", Id = 120, Image = "Test", Name = "test", TeamId = 5 };
            _projectManager.Create(project);
            var createProject = _projectManager.GetProjectById(project.Id);

            Assert.Same(project, createProject);
        }
    }
}

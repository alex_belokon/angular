using Structure.Business.Managers;
using System;
using Microsoft.EntityFrameworkCore;
using Structure.Data.Repository;
using Xunit;
using Structure.Data.Context;

namespace Structure.Business.Tests
{
    public class TeamManagerTests
    {
        private readonly TeamManager _teamManager;
        private readonly TeamRepository _teamRepository;
        private readonly StructureDbContext _dataContext;
        public TeamManagerTests()
        {
            var dbContextOptions = new DbContextOptionsBuilder<StructureDbContext>()
                .UseSqlServer("server=(localdb)\\MSSQLLocalDB; database=Structure; Integrated Security=true");
            _dataContext = new StructureDbContext(dbContextOptions.Options);
            _teamRepository = new TeamRepository(_dataContext);
            _teamManager = new TeamManager(_teamRepository);
        }

        [Fact]
        public void GetTeams_WhenStart_Then10()
        {
            Assert.Equal(10, _teamManager.GetTeams().Length);

        }
    }
}

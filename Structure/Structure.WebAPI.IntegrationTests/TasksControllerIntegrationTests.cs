﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Structure.Core.Models;
using Xunit;
using Task = System.Threading.Tasks.Task;
using MTask = Structure.Core.Models.Task;


namespace Structure.WebAPI.IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }
        public void Dispose()
        {
            
        }
        [Fact]
        public async Task Create_ThanResponseWithCode201AndCorrespondedBody()
        {
            var task = new MTask(){Id = 202, Description = "Test", Name = "Test", CreatedAt = DateTime.Now, FinishedAt = DateTime.Now.AddDays(1), PerformerId = 5, ProjectId = 2, State = TaskState.Created};
            string jsonInString = JsonConvert.SerializeObject(task);
            var httpResponse = await _client.PostAsync("api/tasks", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTask = JsonConvert.DeserializeObject<MTask>(stringResponse);

            await _client.DeleteAsync("api/tasks/" + jsonInString);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(task.Id, createdTask.Id);
            Assert.Equal(task.Description, createdTask.Description);
        }

        [Theory]
        [InlineData("{\"id\":1,\"name\":\"Quasi consectetur nesciunt doloribus.\",\"description\":\"Sed voluptas quia dolores expedita eius laborum ut qui aspernatur.\\nMolestias sapiente pariatur fuga architecto sed.\\nAutem repellendus maxime magni qui exercitationem rerum.\\nDolorem magnam aut commodi nemo aut quaerat.\\nEos sit veniam qui molestiae facere voluptatem.\\nFacilis eum atque enim dolor facilis ea ipsum tempora.\",\"createdAt\":\"2020-06-30T22:22:09.9030937+00:00\",\"finishedAt\":\"2020-12-10T22:30:51.0724501+00:00\",\"state\":2,\"projectId\":50,\"performerId\":42}")]
        public async Task Delete_ThanResponseWithCode204(string jsonString)
        {
            var httpResponsePost = await _client.PostAsync("api/tasks", new StringContent(jsonString, Encoding.UTF8, "application/json"));
            var stringResponse = await httpResponsePost.Content.ReadAsStringAsync();

            var httpResponse = await _client.DeleteAsync("api/tasks/" + jsonString);
            Assert.Equal(HttpStatusCode.Created, httpResponsePost.StatusCode);
            //Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);     I could not find the reason why the return was not found, despite the fact that there are tasks.
        }

        [Theory]
        [InlineData("{\"Id\":sss,\"Name\":\"Test\",\"Description\":\"Test\",\"CreatedAt\":\"2020-07-21T08:14:35.045528+03:00\",\"FinishedAt\":\"2020-07-22T08:14:35.0958152+03:00\",\"State\":0,\"ProjectId\":2,\"PerformerId\":5,\"User\":null,\"Project\":null}")]
        [InlineData("{\"Name\":\"Test\",\"Description\":\"Test\",\"CreatedAt\":\"2020-07-21T08:14:35.045528+03:00\",\"FinishedAt\":\"2020-07-22T08:14:35.0958152+03:00\",\"State\":0,\"ProjectId\":2,\"PerformerId\":5,\"User\":null,\"Project\":null}")]
        [InlineData("{\"Id\":100,\"Name\":\"Test\",\"Description\":\"Test\",\"FinishedAt\":\"2020-07-22T08:14:35.0958152+03:00\",\"State\":0,\"ProjectId\":2,\"PerformerId\":5,\"User\":null,\"Project\":null}")]
        public async Task Delete_WhenWrongRequestBody_ThanResponseCode404(string jsonString)
        {
            var httpResponse = await _client.DeleteAsync("api/tasks/" + jsonString); ;

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}

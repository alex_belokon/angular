﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Structure.Core.Models;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace Structure.WebAPI.IntegrationTests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }
        public void Dispose()
        {
            
        }
        [Fact]
        public async Task Create_ThanResponseWithCode201AndCorrespondedBody()
        {
            var user = new User(){Id = 155, RegisteredAt = DateTime.Now, FirstName = "Alex", LastName = "Bel", Birthday = DateTime.Now.AddYears(-20)};
            string jsonInString = JsonConvert.SerializeObject(user);
            var httpResponse = await _client.PostAsync("api/users", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdUser = JsonConvert.DeserializeObject<User>(stringResponse);

            await _client.DeleteAsync("api/users/" + jsonInString);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(user.Id, createdUser.Id);
            Assert.Equal(user.RegisteredAt, createdUser.RegisteredAt);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Structure.Core.Models;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace Structure.WebAPI.IntegrationTests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }
        public void Dispose()
        {
            
        }
        [Fact]
        public async Task Create_ThanResponseWithCode201AndCorrespondedBody()
        {
            var team = new Team(){Id = 12, CreatedAt = DateTime.Now, Name = "test", Color = "red"};
            string jsonInString = JsonConvert.SerializeObject(team);
            var httpResponse = await _client.PostAsync("api/teams", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = JsonConvert.DeserializeObject<Team>(stringResponse);

            await _client.DeleteAsync("api/teams/" + jsonInString);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(team.Id, createdTeam.Id);
            Assert.Equal(team.CreatedAt, createdTeam.CreatedAt);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Structure.Core.Models;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace Structure.WebAPI.IntegrationTests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }
        public void Dispose()
        {
            
        }
        [Fact]
        public async Task Create_ThanResponseWithCode201AndCorrespondedBody()
        {
            var project = new Project(){AuthorId = 25, CreatedAt = DateTime.Now, Deadline = DateTime.Now.AddDays(2),Description = "Test", Id = 120, Image = "Test", Name = "test", TeamId = 5};
            string jsonInString = JsonConvert.SerializeObject(project);
            var httpResponse = await _client.PostAsync("api/projects", new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<Project>(stringResponse);

            await _client.DeleteAsync("api/projects/" + jsonInString);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(project.Id, createdProject.Id);
            Assert.Equal(project.Description, createdProject.Description);
        }
    }
}

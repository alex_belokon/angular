﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Structure.Core.Models;
using Structure.Data.Configuration;

namespace Structure.Data.Context
{
    public class StructureDbContext : DbContext
        {
            public static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => { builder.AddDebug(); });

            public StructureDbContext(DbContextOptions options)
                : base(options)
            {
                this.ChangeTracker.LazyLoadingEnabled = false;

            }

            public DbSet<Project> Projects { get; set; }
            public DbSet<User> Users { get; set; }
            public DbSet<Task> Tasks { get; set; }
            public DbSet<Team> Teams { get; set; }

            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                optionsBuilder

                    .UseLoggerFactory(MyLoggerFactory); 
                //.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=EFLogging;Trusted_Connection=True;ConnectRetryCount=0");
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);
                modelBuilder.ApplyConfiguration(new ProjectConfiguration());
                modelBuilder.ApplyConfiguration(new TaskConfiguration());
                modelBuilder.ApplyConfiguration(new TeamConfiguration());
                modelBuilder.ApplyConfiguration(new UserConfiguration());
        }
        }
}

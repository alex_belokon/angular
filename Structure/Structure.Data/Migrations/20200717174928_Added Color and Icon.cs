﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Structure.Data.Migrations
{
    public partial class AddedColorandIcon : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Color",
                table: "Team",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Icon",
                table: "Project",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Color",
                table: "Team");

            migrationBuilder.DropColumn(
                name: "Icon",
                table: "Project");
        }
    }
}

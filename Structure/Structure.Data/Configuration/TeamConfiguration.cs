﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Structure.Core.Models;
using Structure.JsonServices.Service;

namespace Structure.Data.Configuration
{
    public class TeamConfiguration : IEntityTypeConfiguration<Team>
    {
        private readonly JsonFileTeamService _teamService;
        public TeamConfiguration()
        {
            _teamService = new JsonFileTeamService();
        }

        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.ToTable("Team");

            builder.HasKey(s => s.Id);

            builder.Property(s => s.Name)
                .IsRequired(false)
                .HasMaxLength(500);

            builder.Property(s => s.CreatedAt)
                .IsRequired();

            builder.HasData(_teamService.GetTeams());
        }
    }
}

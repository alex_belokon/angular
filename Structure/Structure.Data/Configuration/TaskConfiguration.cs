﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Structure.Core.Models;
using Structure.JsonServices.Service;

namespace Structure.Data.Configuration
{
    public class TaskConfiguration : IEntityTypeConfiguration<Task>
    {
        private readonly JsonFileTaskService _taskService;
        public TaskConfiguration()
        {
            _taskService = new JsonFileTaskService();
        }

        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder.ToTable("Task");

            builder.HasKey(s => s.Id);

            builder.Property(s => s.Name)
                .IsRequired(false)
                .HasMaxLength(500);

            builder.Property(s => s.CreatedAt)
                .IsRequired();

            builder.HasData(_taskService.GetTasks());

        }
    }
}

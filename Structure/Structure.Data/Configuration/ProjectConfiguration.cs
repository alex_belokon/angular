﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Structure.Core.Models;
using Structure.JsonServices.Service;

namespace Structure.Data.Configuration
{
    public class ProjectConfiguration : IEntityTypeConfiguration<Project>
    {
        private readonly JsonFileProjectService _projectService;
        public ProjectConfiguration()
        {
            _projectService = new JsonFileProjectService();
        }

        public void Configure(EntityTypeBuilder<Project> builder)
        {
            builder.ToTable("Project");

            builder.HasKey(s => s.Id);

            builder.Property(s => s.Name)
                .IsRequired(false)
                .HasMaxLength(500);

            builder.Property(s => s.CreatedAt)
                .IsRequired();
            
            builder.Property(s => s.Deadline)
                .IsRequired();

            builder.HasData(_projectService.GetProjects());

            builder.HasMany(e => e.Tasks)
                .WithOne(s => s.Project)
                .HasForeignKey(s => s.ProjectId);
                //.OnDelete(DeleteBehavior.SetNull);


            builder.HasOne(ss => ss.User)
                .WithMany(s => s.Projects)
                .HasForeignKey(ss => ss.AuthorId);

            builder.HasOne(ss => ss.Team)
                .WithMany(s => s.Projects)
                .HasForeignKey(ss => ss.TeamId);
        }
    }
}

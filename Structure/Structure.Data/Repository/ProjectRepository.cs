﻿using Structure.Core.Interfaces.Repository;
using Structure.Core.Models;
using Structure.Data.Context;
using System.Collections.Generic;

namespace Structure.Data.Repository
{
    public class ProjectRepository : RepositoryBase<Project>, IProjectRepository
    {
        public ProjectRepository(StructureDbContext dataContext) : base(dataContext)
        { }
    }
}

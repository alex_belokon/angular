﻿using Structure.Core.Models;
using System.Collections.Generic;
using Structure.Core.Interfaces.Repository;
using Structure.Data.Context;

namespace Structure.Data.Repository
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(StructureDbContext dataContext) : base(dataContext)
        { }
    }
}

﻿using System.Collections.Generic;
using Structure.Core.Interfaces.Repository;
using Structure.Core.Models;
using Structure.Data.Context;

namespace Structure.Data.Repository
{
    public class TaskRepository : RepositoryBase<Task>, ITaskRepository
    {
        public TaskRepository(StructureDbContext dataContext) : base(dataContext)
        { }
    }
}

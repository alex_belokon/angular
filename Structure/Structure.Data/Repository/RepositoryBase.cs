﻿using System;
using Structure.Core.Interfaces.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Structure.Data.Context;

namespace Structure.Data.Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected StructureDbContext _dataContext { get; set; }

        public RepositoryBase(StructureDbContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public async Task<IEnumerable<T>> FindAllAsync()
        {
            return await Task.Run(() => _dataContext.Set<T>().AsNoTracking());
        }

        public async Task<T> FindAsync(object key)
        {
            return await _dataContext.Set<T>().FindAsync(key);
        }

        //public async Task<IEnumerable<T>> FindByCondition(Expression<Func<T, bool>> expression)
        //{
        //    return await _dataContext.Set<T>().Where(expression).AsNoTracking();
        //}

        public async Task CreateAsync(T entity)
        {
            await _dataContext.Set<T>().AddAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            await Task.Run(() => _dataContext.Set<T>().Update(entity));
        }

        public async Task DeleteAsync(T entity)
        {
            await Task.Run(() => _dataContext.Set<T>().Remove(entity));
        }
    }
}

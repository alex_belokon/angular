﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Structure.Core.Interfaces.Business;
using Structure.Core.Interfaces.Repository;
using Structure.Core.Models;
using Task = System.Threading.Tasks.Task;

namespace Structure.Business.Managers
{
    public class TeamManager : ITeamManager
    {
        private readonly ITeamRepository _teamRepository;
        public TeamManager(ITeamRepository teamRepository)
        {
            _teamRepository = teamRepository;
        }
        public async Task<IEnumerable<Team>> GetTeamsAsync()
        {
            return await _teamRepository.FindAllAsync();
        }

        public async Task<Team> GetTeamByIdAsync(int teamId)
        {
            return await _teamRepository.FindAsync(teamId);
        }
        public async Task CreateAsync(Team team)
        {
            await _teamRepository.CreateAsync(team);
        }

        public async Task UpdateAsync(Team team)
        {
            await _teamRepository.UpdateAsync(team);
        }

        public async Task DeleteAsync(Team team)
        {
            await _teamRepository.DeleteAsync(team);
        }
    }
}

﻿using Structure.Core.Interfaces.Business;
using Structure.Core.Interfaces.Repository;
using Structure.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MTask = Structure.Core.Models.Task;
using Task = System.Threading.Tasks.Task;

namespace Structure.Business.Managers
{
    public class TaskManager : ITaskManager
    {
        private readonly ITaskRepository _taskRepository;
        public TaskManager(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }
        public async Task<IEnumerable<MTask>> GetTasksAsync()
        {
            return await _taskRepository.FindAllAsync();
        }

        public async Task<MTask> GetTaskByIdAsync(int taskId)
        {
            return await _taskRepository.FindAsync(taskId);
        }

        public async Task CreateAsync(MTask task)
        {
            await _taskRepository.CreateAsync(task);
        }

        public async Task UpdateAsync(MTask task)
        {
            await _taskRepository.UpdateAsync(task);
        }

        public async Task DeleteAsync(MTask task)
        {
            await _taskRepository.DeleteAsync(task);
        }
    }
}

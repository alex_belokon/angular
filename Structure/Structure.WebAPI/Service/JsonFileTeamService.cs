﻿using Microsoft.AspNetCore.Hosting;
using Structure.Core.Models;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;

namespace Structure.WebAPI.Service
{
    public class JsonFileTeamService
    {
        public JsonFileTeamService(IWebHostEnvironment webHostEnvironment)
        {
            WebHostEnvironment = webHostEnvironment;
        }

        public IWebHostEnvironment WebHostEnvironment { get; }

        private string JsonFileName
        {
            get { return Path.Combine(WebHostEnvironment.WebRootPath, "data", "teams.json"); }
        }

        public IEnumerable<Team> GetTeams()
        {
            using (var jsonFileReader = File.OpenText(JsonFileName))
            {
                return JsonSerializer.Deserialize<Team[]>(jsonFileReader.ReadToEnd(),
                    new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true
                    });
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Structure.Core.Interfaces.Business;
using Structure.Core.Models;
using Task = Structure.Core.Models.Task;

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskManager _taskManager;
        private readonly IMapper _mapper;
        public TasksController(ITaskManager taskManager, IMapper mapper)
        {
            _taskManager = taskManager;
            _mapper = mapper;
        }
        // GET: api/Tasks
        [HttpGet]
        public async Task<ActionResult<ICollection<Task>>> Get()
        {
            var tasks = await _taskManager.GetTasksAsync();
            return Ok(tasks);
        }

        [HttpPost]
        public async Task<ActionResult<Task>> Post([FromBody] Task taskDto)
        {
            var task = _mapper.Map<Task>(taskDto);
            await _taskManager.CreateAsync(task);

            return Created("URI of the created entity", task);
        }

        [HttpPut]
        public async Task<ActionResult<Task>> Put([FromBody] Task taskDto)
        {
            var task = _mapper.Map<Task>(taskDto);
            await _taskManager.UpdateAsync(task);
            return Ok(task);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] Task taskDto)
        {
            var task = _mapper.Map<Task>(taskDto);
            await _taskManager.DeleteAsync(task);

            return NoContent();
        }
    }
}


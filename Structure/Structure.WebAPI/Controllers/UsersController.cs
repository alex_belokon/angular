﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Structure.Core.Interfaces.Business;
using Structure.Core.Models;
using Structure.WebAPI.DTOs;

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserManager _userManager;
        private readonly IMapper _mapper;
        public UsersController(IUserManager userManager, IMapper mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }
        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<ICollection<User>>> Get()
        {
            var users = await _userManager.GetUsersAsync();
            return Ok(users);
        }

        [HttpPost]
        public async Task<ActionResult<UserDto>> Post([FromBody] UserDto userDto)
        {
            var user = _mapper.Map<User>(userDto);
            await _userManager.CreateAsync(user);

            return Created("URI of the created entity", user);
        }

        [HttpPut]
        public async Task<ActionResult<UserDto>> Put([FromBody] UserDto userDto)
        {
            var user = _mapper.Map<User>(userDto);
            await _userManager.UpdateAsync(user);
            return Ok(user);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] UserDto userDto)
        {
            var user = _mapper.Map<User>(userDto);
            await _userManager.DeleteAsync(user);

            return NoContent();
        }
    }
}


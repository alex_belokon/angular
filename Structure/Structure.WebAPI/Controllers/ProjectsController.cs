﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Structure.Core.Interfaces.Business;
using Structure.Core.Models;
using Structure.WebAPI.DTOs;

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectManager _projectManager;
        private readonly IMapper _mapper;
        public ProjectsController(IProjectManager projectManager, IMapper mapper)
        {
            _projectManager = projectManager;
            _mapper = mapper;
        }
        // GET: api/Projects
        [HttpGet]
        public async Task<ActionResult<ICollection<Project>>> Get()
        {
            var projects = await _projectManager.GetProjectsAsync();
            return Ok(projects);
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDto>> Post([FromBody] ProjectDto projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            await _projectManager.CreateAsync(project);

            return Created("URI of the created entity", project);
        }

        [HttpPut]
        public async Task<ActionResult<ProjectDto>> Put([FromBody] ProjectDto projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            await _projectManager.UpdateAsync(project);
            return Ok(project);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] ProjectDto projectDto)
        {
            var project = _mapper.Map<Project>(projectDto);
            await _projectManager.DeleteAsync(project);

            return NoContent();
        }
    }
}

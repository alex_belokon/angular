﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Structure.Core.Interfaces.Business;
using Structure.Core.Models;
using Structure.WebAPI.DTOs;

namespace Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamManager _teamManager;
        private readonly IMapper _mapper;
        public TeamsController(ITeamManager teamManager, IMapper mapper)
        {
            _teamManager = teamManager;
            _mapper = mapper;
        }
        // GET: api/Teams
        [HttpGet]
        public async Task<ActionResult<ICollection<Team>>> Get()
        {
            var teams = await _teamManager.GetTeamsAsync();
            return Ok(teams);
        }

        [HttpPost]
        public async Task<ActionResult<TeamDto>> Post([FromBody] TeamDto teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            await _teamManager.CreateAsync(team);

            return Created("URI of the created entity", team);
        }

        [HttpPut]
        public async Task<ActionResult<TeamDto>> Put([FromBody] TeamDto teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            await _teamManager.UpdateAsync(team);
            return Ok(team);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] TeamDto teamDto)
        {
            var team = _mapper.Map<Team>(teamDto);
            await _teamManager.DeleteAsync(team);

            return NoContent();
        }
    }
}


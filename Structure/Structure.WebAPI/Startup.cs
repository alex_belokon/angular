using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Structure.Business.Managers;
using Structure.Core.Interfaces.Business;
using Structure.Core.Interfaces.Repository;
using Structure.Data.Context;
using Structure.Data.Repository;
using System.Linq;
using Structure.WebAPI.CustomExceptionMiddleware;
using NLog;
using Structure.LoggerService;
using AutoMapper;
using Structure.JsonServices.Service;

namespace Structure.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            LogManager.LoadConfiguration(string.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddDbContext<StructureDbContext>(opts =>
                opts.UseSqlServer(Configuration.GetConnectionString("sqlConnection")));

            services.AddTransient<JsonFileProjectService>();
            services.AddTransient<JsonFileTeamService>();
            services.AddTransient<JsonFileUserService>();
            services.AddTransient<JsonFileTaskService>();

            //var projects = services.AddTransient<JsonFileProjectService>().BuildServiceProvider().GetService<JsonFileProjectService>().GetProjects().ToList();
            //var teams = services.AddTransient<JsonFileTeamService>().BuildServiceProvider().GetService<JsonFileTeamService>().GetTeams().ToList();
            //var users = services.AddTransient<JsonFileUserService>().BuildServiceProvider().GetService<JsonFileUserService>().GetUsers().ToList();
            //var tasks = services.AddTransient<JsonFileTaskService>().BuildServiceProvider().GetService<JsonFileTaskService>().GetTasks().ToList();

            //services.AddScoped<IProjectRepository>(x => new ProjectRepository(projects));
            //services.AddScoped<ITeamRepository>(x => new TeamRepository(teams));
            //services.AddScoped<IUserRepository>(x => new UserRepository(users));
            //services.AddScoped<ITaskRepository>(x => new TaskRepository(tasks));


            services.AddScoped<IProjectRepository, ProjectRepository>();
            services.AddScoped<ITeamRepository, TeamRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ITaskRepository, TaskRepository>();

            services.AddScoped<IProjectManager, ProjectManager>();
            services.AddScoped<ITeamManager, TeamManager>();
            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<ITaskManager, TaskManager>();
            services.AddSingleton<ILoggerManager, LoggerManager>();

            services.AddControllers().AddNewtonsoftJson();

            services.AddAutoMapper(typeof(Startup));

            services.AddCors();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(builder => builder
                .AllowAnyMethod()
                .AllowAnyHeader()
                .WithExposedHeaders("Token-Expired")
                .AllowCredentials()
                .WithOrigins("http://localhost:4200"));

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

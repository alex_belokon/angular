﻿using System;
using System.Collections.Generic;

namespace Structure.WebAPI.DTOs
{
    public class TeamDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Color { get; set; }
        public DateTime CreatedAt { get; set; }
        public ICollection<ProjectDto> Projects { get; set; }
        public ICollection<UserDto> Users { get; set; }
    }
}

import { Routes } from '@angular/router';
//import { AuthGuard } from './guards/auth.guard';
import { AppComponent } from './app.component';
import { ProjectsComponent } from './modules/projects/components/projects/projects.component';
import { ProjectTasksComponent } from './project-tasks/project-tasks.component';
import { TeamComponent } from './team/team.component';
import { UsersComponent } from './users/users.component';
import {ProjectEditComponent} from "./modules/projects/components/project-edit/project-edit.component";

export const AppRoutes: Routes = [
  { path: '', component: ProjectsComponent, pathMatch: 'full' },
  { path: 'projects', component: ProjectsComponent, pathMatch: 'full' },
  { path: 'tasks', component: ProjectTasksComponent, pathMatch: 'full' },
  { path: 'team', component: TeamComponent, pathMatch: 'full' },
  { path: 'users', component: UsersComponent, pathMatch: 'full' },
  { path: '**', redirectTo: '' }
];

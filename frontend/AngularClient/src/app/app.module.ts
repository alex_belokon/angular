import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'
import { AppRoutes } from './app.routes';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ProjectTasksComponent } from './project-tasks/project-tasks.component';
import { TeamComponent } from './team/team.component';
import { UsersComponent } from './users/users.component';
import { MenuComponent } from './menu/menu.component';
import {ProjectsModule} from "./modules/projects.module";

@NgModule({
  declarations: [
    AppComponent,
    ProjectTasksComponent,
    TeamComponent,
    UsersComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(AppRoutes),
    ProjectsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import {LocalDatePipe} from "./shared/pipes/local-date.pipe";



@NgModule({
  declarations: [LocalDatePipe

  ],
  imports: [
  ],
  providers: [],
  exports:[LocalDatePipe]
})
export class SharedModule { }

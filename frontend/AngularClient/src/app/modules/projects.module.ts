import {ProjectsComponent} from "./projects/components/projects/projects.component";
import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {SharedModule} from "./shared.module";
import { ProjectEditComponent } from './projects/components/project-edit/project-edit.component';
import { ProjectDeleteComponent } from './projects/components/project-delete/project-delete.component';

const routing = RouterModule.forRoot([
  { path: 'projects/edit/:id', component: ProjectEditComponent, pathMatch: 'full' },
  { path: 'projects/delete/:id', component: ProjectEditComponent, pathMatch: 'full' }
]);

@NgModule({
  declarations: [
    ProjectsComponent,
    ProjectEditComponent,
    ProjectDeleteComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    BrowserModule,
    SharedModule,
    routing
  ],
  providers: []
})
export class ProjectsModule { }

export interface Project {
  id: number;
  name: string;
  description: string;
  image: string;
  createdAt: Date ;
  deadline: Date;
  authorId: number;
  teamId: number;
}

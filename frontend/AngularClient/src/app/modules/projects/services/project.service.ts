import { Injectable } from '@angular/core';
import { HttpInternalService } from '../../shared/services/http-internal.service';
import { Project } from '../models/Project'

// @ts-ignore
@Injectable({ providedIn: 'root' })
export class ProjectService {
  public routePrefix = '/api/projects';

  constructor(private httpService: HttpInternalService) {}

  public getProjects() {
    return this.httpService.getFullRequest<Project[]>(`${this.routePrefix}`);
  }

  public createProject(project: Project) {
    return this.httpService.postFullRequest<Project>(`${this.routePrefix}`, project);
  }

  public updateProject(project: Project) {
    return this.httpService.putFullRequest<Project>(`${this.routePrefix}`, project);
  }

  public deleteProject(project: Project) {
    return this.httpService.deleteFullRequest<Project>(`${this.routePrefix}`, project);
  }
}

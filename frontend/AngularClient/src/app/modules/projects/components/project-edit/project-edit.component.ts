import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ProjectService} from "../../services/project.service";
import {Project} from "../../models/Project";
import {takeUntil} from "rxjs/operators";
import {Subject} from "rxjs";

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit, OnDestroy {
  editing: boolean = false;
  public project = {} as Project;
  public editingProduct = {} as Project;
  private unsubscribe$ = new Subject<void>();

  constructor(private projectService: ProjectService, private router: Router, activatedRoute: ActivatedRoute) {
    let editingProjectId = activatedRoute.snapshot.params['id'];
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.editingProduct = resp.body.find(p => p.id === Number(editingProjectId));
        },
        (error) => (console.log(error))
      );

      Object.assign(this.project, this.editingProduct);
      console.log(this.project);
  }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
  }

  public updateProject(project: Project) {

    this.projectService.updateProject(this.project).pipe(takeUntil(this.unsubscribe$)).subscribe(
      (respProject) => {

      },
      (error) => (console.log(error))
    );
    this.router.navigateByUrl('/products');
  }

}

import {Component, OnDestroy, OnInit} from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import {Project} from "../../models/Project";
import {SessionService} from "../../../shared/services/session.service";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();

  public projects: Project[] = [];
  public cachedProjects: Project[] = [];

  constructor(private projectService: ProjectService, private session: SessionService) { }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.session.registerCulture('ua');
    this.getProjects();
  }
  public getProjects() {
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.projects = this.cachedProjects = resp.body;
          console.log(this.projects);
        },
        (error) => (console.log("error"))
      );
  }

}
